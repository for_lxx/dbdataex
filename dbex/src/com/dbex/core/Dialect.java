package com.dbex.core;

public interface Dialect {
	
	/**
	 * @return 返回该 schema 下的所有表
	 */
	Table[] getAllTables();
	
	/**
	 * 将该列 转为字符串
	 * <p>例：对于oracle数据库，create_date-->to_char(create_date,'yyyy-mm-dd')</p>
	 * @param columnName 列名
	 * @return
	 */
	String toChar(String columnName);
	
	/**
	 * 将字符串转化为日期类型
	 * <p>例：对于oracle数据库，'2013-01-20'-->to_date('2013-01-20','yyyy-mm-dd')</p>
	 * @param columnValue 列值
	 * @return
	 */
	String toDate(String columnValue);
	
	/**
	 * 当前列类型是否为字符串类型
	 * @param columnType
	 * @return
	 */
	boolean isString(String columnType);
	
	/**
	 * 当前列类型是否为数字类型
	 * @param columnType
	 * @return
	 */
	boolean isNumber(String columnType);
	
	/**
	 * 当前列类型是否为日期类型
	 * @param columnType
	 * @return
	 */
	boolean isDate(String columnType);
	
	/**
	 * 根据列的相关参数生成 DDL 语句的列类型
	 * <p>例：对于mysql，varchar(10)</p>
	 * @param column
	 * @return
	 */
	@Deprecated
	String geneColumnType(Column column);

	String getNumberType(Integer pricision, Integer scale);

	String getStringType(boolean isVar, Integer length);

}
