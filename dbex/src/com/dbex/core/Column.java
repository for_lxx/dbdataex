package com.dbex.core;

public class Column {
	
	private String columnName;
	private String columnType;
	
	// -----number----
	private Integer precision; // 有效位数
	private Integer scale; // 小数位数
	// -----number----
	
	// ----string----
	private Integer length;
	private boolean isVar; // 是否可变长
	// ----string----
	
	
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getColumnType() {
		return columnType;
	}
	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}
	public Integer getPrecision() {
		return precision;
	}
	public void setPrecision(Integer precision) {
		this.precision = precision;
	}
	public Integer getScale() {
		return scale;
	}
	public void setScale(Integer scale) {
		this.scale = scale;
	}
	public Integer getLength() {
		return length;
	}
	public void setLength(Integer length) {
		this.length = length;
	}
	public boolean isVar() {
		return isVar;
	}
	public void setVar(boolean isVar) {
		this.isVar = isVar;
	}
	
}
