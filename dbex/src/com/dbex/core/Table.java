package com.dbex.core;


/**
 * 对应数据库中的一张表
 * @author Administrator
 *
 */
public class Table {
	
	/**
	 * 该表原所属的 schema
	 */
	private String schema;  
	/**
	 * 表名
	 */
	private String tableName; 
	/**
	 * 表中的所有列
	 */
	private Column[] columns; 
	
	
	public Table(String srcSchema, String tableName, Column[] columns) {
		super();
		this.schema = srcSchema;
		this.tableName = tableName;
		this.columns = columns;
		log();
	}
	
	private void log() {
		StringBuffer sb = new StringBuffer();
		sb.append("扫描到表："+this.tableName+"(");
		for(Column c : this.columns){
			sb.append(c.getColumnName()).append(',');
		}
		sb.deleteCharAt(sb.length()-1).append(')');
		System.out.println(sb);
	}

	/**
	 * 列1,列2,...
	 * @param format 是否要对Date类型进行格式化转化
	 * @return
	 */
	public String getColumnsName(boolean format, Dialect dialect){
		StringBuilder sb = new StringBuilder();
		for (Column c : this.columns) {
			if(format && dialect.isDate(c.getColumnType())){
				sb.append(dialect.toChar(c.getColumnName())).append(",");
			}else {
				sb.append(c.getColumnName()).append(",");
			}
		}
		return sb.deleteCharAt(sb.length()-1).toString();
	}
	
	
	public String getSchema() {
		return schema;
	}

	public String getTableName() {
		return tableName;
	}

	public Column[] getColumns() {
		return columns;
	}
	
}
