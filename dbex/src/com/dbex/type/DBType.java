package com.dbex.type;

import com.dbex.util.ArrayUtils;

public class DBType {
	
	public static boolean isNumber(String type){
		return ArrayUtils.contains(new String[]{"number","int"}, type);
	}

}
