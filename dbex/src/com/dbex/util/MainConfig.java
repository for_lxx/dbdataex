package com.dbex.util;

import java.util.Properties;

/**
 * 读取配置文件
 * @author Administrator
 *
 */
public class MainConfig {
	private static final Properties prop = new Properties();
	static {
		try {
			prop.load(DBUtil.class.getClassLoader().getResourceAsStream("com/dbex/config/db.properties"));
			init();
		} catch (Exception e) {
			throw new RuntimeException("配置文件读取失败",e);
		}
	}
	
	/**
	 * 获取配置文件的参数值
	 * @param paramName 参数名
	 * @return
	 */
	public static String getValue(String paramName){
		return prop.getProperty(paramName);
	}
	
	/**
	 * 读取配置文件,设置静态变量
	 */
	public static void init(){
		String showSql = MainConfig.getValue("show_sql");
		if("true".equals(showSql))
			StaticConstant.SHOW_SQL=true;
		String outputFile = MainConfig.getValue("output_file");
		if(null!=outputFile)
			StaticConstant.DEFAULT_EXPORT_FILE = outputFile;
	}

}
