package com.dbex.util;

public class ArrayUtils {
	
	/**
	 * 在arr数组中是否包含val
	 * @param arr 数组
	 * @param val 查找的值
	 * @return 若val为null，则返回false
	 */
	public static boolean contains(Object[] arr, Object val){
		if(val==null) return false;
		for(Object obj : arr){
			if(val.equals(obj)) return true;
		}
		return false;
	}

}
