package com.dbex.util;

import java.sql.ResultSet;


public abstract class RowMapper<T> {
	public abstract T rowMapper(ResultSet rs) throws Exception ;
}
